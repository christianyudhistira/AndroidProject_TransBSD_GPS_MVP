package com.example.gadget.transbsd_gps_sender_marshmallow_mvp;

import android.content.Context;

/**
 * Created by gadget on 1/23/18.
 */

public class OperationalPresenterImpl implements OperationalPresenter, OperationalProcess.OnOperationalFinishedListener {

    private OperationalView operationalView;
    private OperationalProcess operationalProcess;

    public OperationalPresenterImpl(OperationalView view, Context context) {
        this.operationalView = view;
        operationalProcess = new OperationalProcessImpl(this, context);
    }

    @Override
    public void onStartGPSService() {
        operationalProcess.processActivateGPSUpdate();
    }

    @Override
    public void onStopGPSService() {
        operationalProcess.processRemoveGPSUpdate();
    }

    @Override
    public void onSuccess(String lat, String lang) {
        operationalView.showLocation(lat, lang);
    }

    @Override
    public void onFailed() {

    }
}
