package com.example.gadget.transbsd_gps_sender_marshmallow_mvp;

/**
 * Created by gadget on 1/23/18.
 */

public interface OperationalView {
    void showLocation(String latitude, String longitude);
}
