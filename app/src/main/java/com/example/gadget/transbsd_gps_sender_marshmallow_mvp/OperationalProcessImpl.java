package com.example.gadget.transbsd_gps_sender_marshmallow_mvp;

import android.content.Context;

/**
 * Created by gadget on 1/23/18.
 */

public class OperationalProcessImpl implements OperationalProcess {

    private OperationalLocationService locationService;

    public OperationalProcessImpl(OnOperationalFinishedListener listener, Context c) {
        locationService = new OperationalLocationServiceImpl(listener, c);
    }


    @Override
    public void processActivateGPSUpdate() {
        locationService.startLocationService();
    }

    @Override
    public void processRemoveGPSUpdate() {
        locationService.stopLocationService();
    }
}
