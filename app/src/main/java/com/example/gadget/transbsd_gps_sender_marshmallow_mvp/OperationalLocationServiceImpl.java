package com.example.gadget.transbsd_gps_sender_marshmallow_mvp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by gadget on 1/23/18.
 */

public class OperationalLocationServiceImpl implements OperationalLocationService {

    private Context context;
    private OperationalProcess.OnOperationalFinishedListener listener;

    public OperationalLocationServiceImpl(OperationalProcess.OnOperationalFinishedListener listener, Context c) {
        this.listener = listener;
        this.context = c;
    }

    @Override
    public void startLocationService() {
        Intent intent = new Intent(context, GPSService.class);
        context.startService(intent);

        LocalBroadcastManager.getInstance(context).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String latitude = intent.getStringExtra(GPSService.EXTRA_LATITUDE);
                        String longitude = intent.getStringExtra(GPSService.EXTRA_LONGITUDE);

                        listener.onSuccess(latitude, longitude);
                    }
                }, new IntentFilter(GPSService.ACTION_LOCATION_BROADCAST)
        );
    }

    @Override
    public void stopLocationService() {
        Intent intent = new Intent(context, GPSService.class);
        context.stopService(intent);
    }
}
