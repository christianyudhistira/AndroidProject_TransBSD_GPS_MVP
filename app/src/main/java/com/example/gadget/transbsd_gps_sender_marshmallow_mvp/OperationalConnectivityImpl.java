package com.example.gadget.transbsd_gps_sender_marshmallow_mvp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by baliteam on 07/03/18.
 */

public class OperationalConnectivityImpl {

    private static Context context;

    public OperationalConnectivityImpl(Context c) {
        super();
        this.context = c;
    }

    public static boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
