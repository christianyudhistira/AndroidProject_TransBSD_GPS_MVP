package com.example.gadget.transbsd_gps_sender_marshmallow_mvp;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by baliteam on 09/03/18.
 */

public class OperationalPostRequestImpl {

    private RequestQueue queue;
    private String url = "https://development-dot-qluster-sml.appspot.com/api/gps/jei5etot";
    private StringRequest stringRequest;
    private Context context;

    public OperationalPostRequestImpl(Context c) {
        queue = Volley.newRequestQueue(c);
        this.context = c;
    }

    // send POST request
    public void sendPOST(final String lat, final String lng,
                         final String platNum, final String carName, final String ignitionStat, final String carType) {

        // Request a string response
        stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(context, response, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "FAILED", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("number", platNum);
                params.put("name", carName);
                params.put("latitude", lat);
                params.put("longitude", lng);
                params.put("ignition", ignitionStat);
                params.put("course", "90");
                params.put("type", carType);
                params.put("people_count", "20");

                return params;
            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String,String> params = new HashMap<String, String>();
//                params.put("Content-Type","application/x-www-form-urlencoded");
//                return params;
//            }
        };

        // Request a JSON response - belom kelarr
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//
//                    }
//                }
//        ) {
//
//        };

        queue.add(stringRequest);
    }
}
