package com.example.gadget.transbsd_gps_sender_marshmallow_mvp;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by gadget on 1/23/18.
 */

public class GPSService extends Service implements GPSServiceStat {

    public static final String
            ACTION_LOCATION_BROADCAST = GPSService.class.getName() + "LocationBroadcast",
            EXTRA_LATITUDE = "extra_latitude",
            EXTRA_LONGITUDE = "extra_longitude";

    // service definitions
    private String latMessage;
    private String longMessage;
    private OperationalGPSLocation operationalGPSLocation;

    // multithread definitions
    private HandlerThread handlerThread;
    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;

    // MQTT definitions
    private OperationalMQTTImpl operationalMQTT;

    // POST request definitions
    private OperationalPostRequestImpl operationalPostRequest;

    // buffer definitions
    private ArrayList<HashMap<String, String>> gpsData = new ArrayList<HashMap<String, String>>();
    private int looping = 0;

    // connectivity definitions
    private OperationalConnectivityImpl operationalConnectivity;

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    }

    // internal storage definitions
    private String FILENAME = "vehicle_data";
    private FileInputStream fin;
    private ObjectInputStream ois;

    @Override
    public void onCreate() {
        // initialize GPSLocation
        operationalGPSLocation = new OperationalGPSLocationImpl(getApplicationContext(), this);
        operationalConnectivity = new OperationalConnectivityImpl(getApplicationContext());
        operationalMQTT = new OperationalMQTTImpl(getApplicationContext());
        operationalPostRequest = new OperationalPostRequestImpl(getApplicationContext());

        // MQTT client setup
//        operationalMQTT.mqttStart();

        // Create and Start the HandlerThread
        handlerThread = new HandlerThread("HandlerThread");
        handlerThread.start();
        // Get the HandlerThread's Looper
        mServiceLooper = handlerThread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mServiceHandler.post(new Runnable() {
            @Override
            public void run() {
                operationalGPSLocation.RequestGPSUpdate();
            }
        });

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        handlerThread.quitSafely();
        operationalGPSLocation.RemoveGPSUpdate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationReady() {
        String[] vehicleData = null;

        // load Vehicle data from internal storage
        try {
            fin = openFileInput(FILENAME);
            ois = new ObjectInputStream(fin);
            vehicleData = (String[]) ois.readObject();
            ois.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        latMessage = String.valueOf(operationalGPSLocation.getLastLatitude());
        longMessage = String.valueOf(operationalGPSLocation.getLastLongitude());

        // send localBroadcast message
        Intent intent = new Intent(ACTION_LOCATION_BROADCAST);
        intent.putExtra(EXTRA_LATITUDE, latMessage);
        intent.putExtra(EXTRA_LONGITUDE, longMessage);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

        // insert GPS location into the Buffer
        addElement(latMessage, longMessage);

        // check internet connectivity
        boolean isConnected = checkConnectivity();
        if (isConnected && vehicleData != null) { // connected to the internet
            int count;
            int bufferCounter = countElement();
            for (count=0; count<bufferCounter; count++) {
                // HTTP Post request
                operationalPostRequest.sendPOST(gpsData.get(count).get("lat"), gpsData.get(count).get("long"),
                        vehicleData[0].toString(), vehicleData[1].toString(), vehicleData[2].toString(), vehicleData[3].toString());
            }

            gpsData.clear();
        }
        else { // isn't connected to the internet
            Toast.makeText(GPSService.this, "ga konek", Toast.LENGTH_LONG).show();
        }
    }

    // store GPS data into the buffer
    private void addElement(String lat, String lng) {
        HashMap<String, String> hashElement = new HashMap<String,String>();
        hashElement.put("lat", lat);
        hashElement.put("long", lng);
        gpsData.add(hashElement);
    }

    // count GPS data in the Buffer
    private int countElement() {
        return gpsData.size();
    }

    // check internet connectivity
    private boolean checkConnectivity() {
        return operationalConnectivity.isConnected();
    }
}