package com.example.gadget.transbsd_gps_sender_marshmallow_mvp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by gadget on 1/24/18.
 */

public class GPSTriggerReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent myService = new Intent(context, GPSService.class);
        context.startService(myService);
    }
}
