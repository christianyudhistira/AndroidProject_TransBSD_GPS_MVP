package com.example.gadget.transbsd_gps_sender_marshmallow_mvp;

import android.content.Context;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 * Created by baliteam on 08/03/18.
 */

public class OperationalMQTTImpl {
    private MqttAndroidClient client;
    private String clientId;

    private final String MQTTHOST = "tcp://103.11.133.150:1883";
    private final String latTopic = "/latitude";
    private final String longTopic = "/longitude";

    public OperationalMQTTImpl(Context c) {
        clientId = MqttClient.generateClientId();
        client = new MqttAndroidClient(c, MQTTHOST, clientId);
    }

    public void mqttStart() {
        try {
            IMqttToken token = client.connect();
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
//                    Log.d(TAG, "onSuccess");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems
//                    Log.d(TAG, "onFailure");
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void mqttPublish(String lat, String lng) {
        try {
            client.publish(latTopic, lat.getBytes(), 0, false);
            client.publish(longTopic, lng.getBytes(), 0, false);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public boolean mqttCheckConnectivity() {
        return client.isConnected();
    }
}
