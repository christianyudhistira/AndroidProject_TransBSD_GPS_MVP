package com.example.gadget.transbsd_gps_sender_marshmallow_mvp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MainActivity extends AppCompatActivity implements OperationalView {

    private TextView latitudeTextView;
    private TextView longitudeTextView;
    private TextView returnTextView;
    private TextView nameTextView;
    private TextView platTextView;
    private OperationalPresenter operationalPresenter;

    private static int PERMISSION_ALL = 1;
    private static final String TAG = "IMEI";
    private String[] PERMISSIONS = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION};

    // internal storage definitions
    private String FILENAME = "vehicle_data";
    private FileOutputStream fout;
    private FileInputStream fin;
    private ObjectOutputStream oos;
    private ObjectInputStream ois;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        latitudeTextView = (TextView)findViewById(R.id.latitudeTextView);
        longitudeTextView = (TextView)findViewById(R.id.longitudeTextView);
        returnTextView = (TextView)findViewById(R.id.returnTextView);
        nameTextView = (TextView) findViewById(R.id.nameTextView);
        platTextView = (TextView) findViewById(R.id.platTextView);
        operationalPresenter = new OperationalPresenterImpl(this, MainActivity.this);

        // check permission to access GPS
        if (!hasPermissions(PERMISSIONS))
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
    }

    private boolean hasPermissions( String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null)
            for (String permission : permissions)
                if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED)
                    return false;

        return true;
    }

    @Override
    protected void onResume() { // start GPS service
        super.onResume();

        String[] stringData = null;
        try {
            fin = openFileInput(FILENAME);
            ois = new ObjectInputStream(fin);
            stringData = (String[]) ois.readObject();
            ois.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        if (stringData != null) {
            nameTextView.setText(stringData[1].toString());
            platTextView.setText(stringData[0].toString());
        }

        operationalPresenter.onStartGPSService();
    }

    @Override
    public void showLocation(String latitude, String longitude) {
        latitudeTextView.setText(latitude);
        longitudeTextView.setText(longitude);
    }

    // Stop Service for debugging
    public void stopService(View view) {
        operationalPresenter.onStopGPSService();
    }

    // Setting Transport
    public void settingTransport(View view) {
        // stop GPS service
        operationalPresenter.onStopGPSService();
        Intent intent = new Intent(this, SettingVehicleActivity.class);
        startActivity(intent);
    }
}
