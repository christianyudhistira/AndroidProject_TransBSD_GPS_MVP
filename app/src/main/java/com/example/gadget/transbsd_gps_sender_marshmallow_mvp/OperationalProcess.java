package com.example.gadget.transbsd_gps_sender_marshmallow_mvp;

/**
 * Created by gadget on 1/23/18.
 */

public interface OperationalProcess {

    interface OnOperationalFinishedListener {
        void onSuccess(String lat, String lang);

        void onFailed();
    }

    void processActivateGPSUpdate();

    void processRemoveGPSUpdate();
}
