package com.example.gadget.transbsd_gps_sender_marshmallow_mvp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SettingVehicleActivity extends AppCompatActivity {

    private EditText platNumber;
    private EditText vehicleName;
    private Switch ignitionStatus;
    private Spinner vehicleType;

    // internal storage definitions
    private String FILENAME = "vehicle_data";
    private FileOutputStream fout;
    private ObjectOutputStream oos;

    private String ignitionValue = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_vehicle);

        platNumber = (EditText) findViewById(R.id.editTextNumber);
        vehicleName = (EditText) findViewById(R.id.editTextName);
        ignitionStatus = (Switch) findViewById(R.id.switch1);
        vehicleType = (Spinner) findViewById(R.id.spinner);

        ignitionValue = "0";

        ignitionStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    ignitionValue = "1";
                } else {
                    ignitionValue = "0";
                }
            }
        });
    }

    // save Vehicle parameter into local storage
    public void saveButton(View view) {

        // store Vehicle data into buffer
        String[] streamData = new String[4];
        streamData[0] = new String(platNumber.getText().toString());
        streamData[1] = new String(vehicleName.getText().toString());
        streamData[2] = ignitionValue;
        streamData[3] = vehicleType.getSelectedItem().toString();

        // move Vehicle data from buffer to local storage
        try {
            fout = openFileOutput(FILENAME, Context.MODE_PRIVATE);
            oos = new ObjectOutputStream(fout);
            oos.writeObject(streamData);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // return to homepage
        finish();
    }
}
