Android GPS Locator for TransBSD        
================================

This project build an Android based application which is mainly used to track movement of the bus position using 
Android internal GPS.

Development Environment
-----------------------
1. Computer OS      : ubuntu 16.0.4 LTS 4.13.0-17-generic
2. IDE              : Android Studio 3.0.1, Java
3. Android Gadget   : Xiaomi Redmi 5A, Nougat 7.1.2

cara membuka android studio:
1. cd /usr/local/android-studio/bin/
2. ./studio.sh

Program the Developer and Release APK
--------------------------------------
- Developer
    1. Open the project file in Android Studio
    2. Press Ctrl+F9 to build the project. makesure the building process is successful (check Messages option on a bottom page)
    3. Connect the target smartphone with computer via USB. activate the USB debugger in a target phone.
    4. Continue to program the APK by preshing Ctrl+F10. Wait until application screen is displayed
    
- Release
    - reference web: http://www.techotopia.com/index.php/Generating_a_Signed_Release_APK_File_in_Android_Studio
    1. Local Release APK Directory: Android_Project_Location/app/release/app-release.apk
    2. Current keystore data:
        - keystore password: keystransbsd123
        - key password: keytransbsd123

Project Design
--------------
- class diagram filename: TransBSD_Android-App_Class-Diagram_MVP.png
- class diagram project file: TransBSD_Android-App_Class-Diagram_MVP, open with www.draw.io